#lang racket

(provide crazy2val)

(define (crazy2val crazy2)
  (letrec
       ; symbol2int: get integer from symbol
      ((symbol2int
        (lambda (radix)
          (cond [(eq? radix 'p) 1]
                [(eq? radix 'z) 0]
                [(eq? radix 'n) -1])))
       ; proc: main process
       ; parameter crazy2: the input sequence
       ; parameter exp2: the power of 2
       ; calculate d1 * 2^0 + d2 * 2^1 + ... + dn * 2^n
       (proc
        (lambda (crazy2 exp2)
          (cond [(null? crazy2) 0]
                [(symbol? crazy2) (* (symbol2int crazy2) exp2)]
                [else (+ (* (symbol2int (car crazy2)) exp2)
                         (proc (cdr crazy2) (* 2 exp2)))]))))
    (proc crazy2 1)))
