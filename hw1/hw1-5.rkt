#lang racket

(provide crazy2add)

(define (crazy2add lhs rhs)
  (letrec
       ; radixadd: add two radix and carry, get (values quotient carry)
      ((radixadd
        (lambda (lradix rradix carry)
          (cond [(eq? lradix rradix) (values carry lradix)]
                [(eq? 'z lradix) (radixadd rradix carry 'z)]
                [(eq? 'z rradix) (cond [(eq? 'z carry) (values lradix 'z)]
                                       [else (radixadd lradix carry 'z)])]
                [else (values carry 'z)])))
       ; peek: get head element of list and list of others
       (peek
        (lambda (crazy2)
          (cond [(null? crazy2) (values 'z '())]
                [(symbol? crazy2) (values crazy2 '())]
                [else (values (car crazy2) (cdr crazy2))])))

       ; proc: main process
       (proc
        (lambda (lhs rhs carry)
          ; 1) add two n-th digit and carry
          ;    get symbols of quotient and carry
          (let*-values
              ([(ltop lrest) (peek lhs)]
               [(rtop rrest) (peek rhs)]
               [(quotient carry) (radixadd ltop rtop carry)])
            ; 2) append just quotient until lhs and rhs are null
            (if (and (null? lrest) (null? rrest))
                ; 3) finally, append last carry
                (cons quotient carry) 
                (cons quotient (proc lrest rrest carry)))))))
    ; initial carry is 'z
    (proc lhs rhs 'z)))
