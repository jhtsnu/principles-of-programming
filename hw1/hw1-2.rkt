#lang racket

(provide t2)

(define (t2 n)
  (letrec
      ; ones: returns 'n' times "1" 
      ((ones
        (lambda (n)
          (if (zero? n)
              ""
              (string-append "1" (ones (- n 1))))))
       ; proc: main process
       (proc
        (lambda (n)
          (if (zero? n)
              "0"
              (string-append (t2 (- n 1))
                             "0"
                             (ones n))))))
    ; (t2 n) and (t2 -n) are equal
    (proc (abs n))))
