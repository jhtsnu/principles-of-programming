exception Todo

type exp =
  | Num of int
  | Add of exp * exp
  | Minus of exp
  | Read
  | If of exp * exp * exp
  | Repeat of exp * exp
type var = string
type tag = string
type cmd =
  | HasNum of var * int
  | HasVar of var * var
  | HasSum of var * var * var
  | HasSub of var * var * var
  | HasRead of var
  | Say of var
  | Goto of tag * var
  | Tag of tag * cmd
  | Seq of cmd * cmd

let rec string_of_cmd (c:cmd) : string =
  match c with
  | HasNum (x,i) -> x^" has "^(string_of_int i)
  | HasVar (x,y) -> x^" has "^y
  | HasSum (x,y,z) -> x^" has "^y^"+"^z
  | HasSub (x,y,z) -> x^" has "^y^"-"^z
  | HasRead x -> x^" has read"
  | Say x -> "say "^x
  | Goto (t,x) -> "goto "^t^" on "^x
  | Tag (t,c1) -> t^": "^(string_of_cmd c1)
  | Seq (c1,c2) -> "("^(string_of_cmd c1)^" ;\n"^(string_of_cmd c2)^")"
let pprint_cmd (c:cmd) : unit = print_endline (string_of_cmd c)

type indent = int
let string_of_indent (idt:indent) : string = String.make idt ' '
let rec string_of_exp (idt:indent) (e:exp) : string =
  (string_of_indent idt)^
    (match e with
     | Num i -> string_of_int i
     | Add (e1,e2) ->
        "+\n"^
          (string_of_exp (idt+2) e1)^"\n"^
            (string_of_exp (idt+2) e2)
     | Minus e1 -> "-\n"^(string_of_exp (idt+2) e1)
     | Read -> "read"
     | If (e1,e2,e3) ->
        "if\n"^
          (string_of_exp (idt+2) e1)^"\n"^
            (string_of_exp (idt+2) e2)^"\n"^
              (string_of_exp (idt+2) e3)
     | Repeat (e1,e2) ->
        "repeat\n"^
          (string_of_exp (idt+2) e1)^"\n"^
            (string_of_exp (idt+2) e2)
    )
let pprint_exp (e:exp) : unit = print_endline (string_of_exp 0 e)

module VarGen = struct
  let var_count = ref 0
  let get(): string =
    (var_count := !var_count + 1);
    "x" ^ string_of_int !var_count
end

module TagGen = struct
  let tag_count = ref 0
  let get(): string =
    (tag_count := !tag_count + 1);
    "t" ^ string_of_int !tag_count
end

module VarStackMemory = struct
  let hash_table = Hashtbl.create 42
  let mem key = Hashtbl.mem hash_table key
  let set key value =
    if mem key then
      let stack = Hashtbl.find hash_table key in
      let _ = Stack.pop stack in
      Stack.push value stack
    else
      let stack = Stack.create() in
      (Stack.push value stack);
      Hashtbl.replace hash_table key stack
  let top key =
    Stack.top (Hashtbl.find hash_table key)
  let pushSFP = (* stack frame pointer *)
    Hashtbl.iter (fun key stack -> Stack.push (-99999999) stack) hash_table
  let popSFP =
    Hashtbl.iter (fun key stack ->
                  let _ = Stack.pop stack in ()) hash_table
end

module TagMemory = struct
  let hash_table = Hashtbl.create 42
  let set key value = Hashtbl.replace hash_table key value
  let get key = Hashtbl.find hash_table key
  let mem key = Hashtbl.mem hash_table key
end

let transform (e:exp) : cmd =
  let rec setVar (v:var) (e:exp): cmd =
    match e with
    | Num (n) -> HasNum (v, n)
    | Add (e1, e2) ->
       let v1 = VarGen.get() in
       let v2 = VarGen.get() in
       Seq ((setVar v1 e1), Seq ((setVar v2 e2), HasSum (v, v1, v2)))
    | Minus (e1) ->
       let v1 = VarGen.get() in
       let v2 = VarGen.get() in
       Seq ((setVar v1 e1), Seq (HasNum (v2, 0), HasSub (v, v2, v1)))
    | Read -> HasRead v
    | If (e1, e2, e3) ->
       let v1 = VarGen.get() in
       let v2 = VarGen.get() in
       let t1 = TagGen.get() in
       let t2 = TagGen.get() in
       Seq (setVar v1 e1,
            Seq (HasNum (v2, 1),
                 Seq (Goto (t1, v1),
                      Seq (setVar v e3,
                           Seq (Goto (t2, v2),
                                Seq (Tag (t1, (Say v1)),
                                     Seq (setVar v e2,
                                          (Tag (t2, (Say v))))))))))
    | Repeat (e1, e2) ->
       let t1 = TagGen.get() in
       let t2 = TagGen.get() in
       let v1 = VarGen.get() in
       let v2 = VarGen.get() in
       let v3 = VarGen.get() in
       Seq (setVar v1 e1,
            Seq (setVar v2 e2,
                 Seq (HasNum (v, 0),
                      Seq (HasNum (v3, 1),
                           Seq (Tag (t1, (Say v)),
                                Seq (Goto (t2, v1),
                                     Seq (HasSum (v, v, v2),
                                          Seq (HasSub (v1, v1, v3),
                                               Seq (Goto (t1, v1),
                                                    (Tag (t2, (Say v))))))))))))
  in
  let v = VarGen.get() in
  Seq (setVar v e, Say v)

let rec check_cmd (c:cmd) : bool =

  let rec read_tag (c:cmd) =
    match c with
    | Tag (tg, tc) ->
       TagMemory.set tg tc
    | Seq (c1, c2) ->
       (match c1 with
        | Tag (tg, tc) ->
           (TagMemory.set tg (Seq (tc, c2)));
           read_tag c2
        | _ -> (read_tag c1); read_tag c2)
    | _ -> ()
  in

  let rec _check_cmd (c:cmd) : bool =
    match c with
    | HasNum (v, n) ->
       (VarStackMemory.set v n);
       true
    | HasVar (v1, v2) -> (
      try
        (VarStackMemory.set v1 (VarStackMemory.top v2));
        true
      with Not_found -> false )
    | HasSum (v1, v2, v3) -> (
      try
        (VarStackMemory.set v1 ((VarStackMemory.top v2) + (VarStackMemory.top v3)));
        true
      with Not_found ->
        false )
    | HasSub (v1, v2, v3) -> (
      try
        (VarStackMemory.set v1 ((VarStackMemory.top v2) - (VarStackMemory.top v3)));
        true
      with Not_found -> false )
    | HasRead (v) ->
       true
    | Say (v) ->
       VarStackMemory.mem v
    | Goto (tg, v) -> (
      try
        if (VarStackMemory.top v) = 0 then
          true
        else
          _check_cmd (TagMemory.get tg)
      with Not_found -> false )
    | Tag (tg, c) ->
       _check_cmd c
    | Seq (c1, c2) ->
       match c1 with
       | Goto (tg, v) -> (
         try
           if (VarStackMemory.top v) = 0 then
             _check_cmd c2
           else
             _check_cmd (TagMemory.get tg)
         with Not_found -> false )
       | Tag (tg, c) ->
          (match c with
           | HasRead (v) ->
              let result = ref true in
              (for i = -100 to 100 do
                 let _ = VarStackMemory.pushSFP in
                 let _ = (VarStackMemory.set v i) in
                 let _ = (result := !result && (_check_cmd c2)) in
                 VarStackMemory.popSFP
               done);
              !result
           | _ -> (_check_cmd c) && (_check_cmd c2))
       | HasRead (v) ->
          let result = ref true in
          (for i = -100 to 100 do
             let _ = VarStackMemory.pushSFP in
             let _ = (VarStackMemory.set v i) in
             let _ = (result := !result && (_check_cmd c2)) in
             VarStackMemory.popSFP
           done);
          !result
       | _ -> (_check_cmd c1) && (_check_cmd c2)
  in
  (read_tag c);
  try
    (_check_cmd c)
  with Stack_overflow -> true

exception MinusRepeat

let check_exp (e:exp) : bool =
  let rec getInt (e:exp) : int * int = 
    match e with
    | Num (n) -> (n, n)
    | Add (e1, e2) -> 
       let (e1min, e1max) = (getInt e1) in
       let (e2min, e2max) = (getInt e2) in
       (e1min + e2min, e1max + e2max)
    | Minus (e1) -> 
       let (e1min, e1max) = (getInt e1) in
       (min (-e1min)  (-e1max), max (-e1min) (-e1max))
    | Read -> (-100, 100)
    | If (e1, e2, e3) ->
       let (e1min, e1max) = (getInt e1) in
       if (e1min <= 0 && 0 <= e1max) then
         let _ = (getInt e3) in
         (getInt e2)
       else if (e1min = 0 && 0 = e1max) then
         (getInt e3)
       else
         (getInt e2)
    | Repeat (e1, e2) ->
       let (e1min, e1max) = (getInt e1) in
       if e1min >= 0 then
         let (e2min, e2max) = (getInt e2) in
         (e2min * e1max, e2max * e1max)
       else raise MinusRepeat
  in
  try
    let _ = getInt e in
    true
  with MinusRepeat -> false
