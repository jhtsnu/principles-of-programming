exception TODO

(* Project #1
 * You can assume that no three points are collinear.
 *)
module type DUSTSTORM =
  sig
    type robot = string (* robot’s name *)
    type shelter = int (* shelter’s id number *)
    type location = int * int (* 화성에서의 위도와 경도 (coordinate) *)
    type robot_locs = (robot * location) list
    type shelter_locs = (shelter * location) list
    val shelterAssign: robot_locs -> shelter_locs -> (robot * shelter) list
  end

module Duststorm = struct
  type robot = string (* robot’s name *)
  type shelter = int (* shelta’s id number *)
  type location = int * int (* 화성에서의 위도와 경도 (coordinate) *)
  type robot_locs = (robot * location) list
  type shelter_locs = (shelter * location) list

  let is_converged = ref false

  let rec shelterAssign r_locs sh_locs =
    List.map (fun (pair) -> let ((robot_name, _), (shelter_name, _)) = pair in
                            (robot_name, shelter_name))
             (iterate (init_matching r_locs sh_locs))

  and iterate iterate_pairs =
    (is_converged := true);
    let new_pairs = rematch iterate_pairs in

    if !is_converged = true then
      new_pairs
    else
      iterate new_pairs

  and rematch pairs =
    match pairs with
    | [] -> pairs
    | pair::pair_remains ->
       let (pair, pair_remains) = loop pair pair_remains in
       pair::(rematch pair_remains)

  and loop pair pairs =
    match pairs with
    | [] -> (pair, [])
    | pair_head::pair_remains ->
       let (untangled_pair, pair_head) = untangle pair pair_head in
       if untangled_pair = pair then
         let (looped_pair, looped_pair_remains) = (loop pair pair_remains) in
         (looped_pair, pair_head::looped_pair_remains)
       else
         (untangled_pair, pair_head::pair_remains)

  and init_matching r_locs sh_locs =
    match (r_locs, sh_locs) with
    | (_, []) -> []
    | ([], _) -> []
    | (r_loc_head::r_loc_remains, sh_loc_head::sh_loc_remains) ->
       (r_loc_head, sh_loc_head)::(init_matching r_loc_remains sh_loc_remains)

  and untangle pair1 pair2 =
    if intersect pair1 pair2 then
      ((is_converged := false);
       match (pair1, pair2) with
       | ((r_loc1, sh_loc1), (r_loc2, sh_loc2)) -> ((r_loc1, sh_loc2), (r_loc2, sh_loc1)))
    else (pair1, pair2)

  and intersect pair1 pair2 =
    match (pair1, pair2) with
    | (((_, xy1), (_, xy2)), ((_, xy3), (_, xy4))) ->
       (ccw xy1 xy2 xy3) * (ccw xy1 xy2 xy4) <= 0 &&
         (ccw xy3 xy4 xy1) * (ccw xy3 xy4 xy2) <= 0

  and ccw pos1 pos2 pos3 =
    match (pos1, pos2, pos3) with
    | ((x1, y1), (x2, y2), (x3, y3)) ->
       ((x1 * y2) + (x2 * y3) + (x3 * y1)) - ((x1 * y3) + (x2 * y1) + (x3 * y2))

end
