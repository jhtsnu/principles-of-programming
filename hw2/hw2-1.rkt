#lang racket

(provide zipper)

(define (zipper lhs rhs)
  (if (null? lhs)
      rhs ; return rhs if lhs is '()
      (if (null? rhs)
          lhs ; return lhs if rhs is '()
          (append (list (car lhs) (car rhs))       ; if lhs and rhs are not '(),
                  (zipper (cdr lhs) (cdr rhs)))))) ; shuffle two lists alternately
