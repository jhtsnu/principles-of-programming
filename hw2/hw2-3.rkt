#lang racket

(provide iter)

(define (iter n f)
  (letrec
      ((proc
        (lambda (n f)
          (if (zero? n)
              identity ; if n is zero, return identity( (lambda(x) x) )
              (lambda (parameter) ; iter 함수를 적용한 결과를 다시 f 와 연산한 결과를 리턴하는 함수를 리턴한다.
                (f ((iter (- n 1) f) parameter)))))))
    (proc (abs n) f))) ; n 은 음수의 경우도 고려한다.
