type team = Korea | France | Usa | Brazil | Japan | Nigeria | Cameroon
          | Poland | Portugal | Italy | Germany | Norway | Sweden | England
          | Argentina

type tourna = LEAF of team
            | NODE of tourna * tourna

let rec drop (tn:tourna) (loser:team): string =

  let rec parenize (tn: tourna): string =
    match tn with
    | LEAF tm ->
       (match tm with
        | Korea -> "Korea"
        | France -> "France"
        | Usa -> "Usa"
        | Brazil -> "Brazil"
        | Japan -> "Japan"
        | Nigeria -> "Nigeria"
        | Cameroon -> "Cameroon"
        | Poland -> "Poland"
        | Portugal -> "Portugal"
        | Italy -> "Italy"
        | Germany -> "Germany"
        | Norway -> "Norway"
        | Sweden -> "Sweden"
        | England -> "England"
        | Argentina -> "Argentina")
    | NODE (leaf_tn, right_tn) ->
       "(" ^ (parenize leaf_tn) ^ " " ^ (parenize right_tn) ^ ")"
  in

  match tn with
  | LEAF tm -> if tm = loser then "" else parenize (LEAF tm)
  | NODE (left_tn, right_tn) ->
     if (drop left_tn loser) = "" then parenize right_tn
     else if (drop right_tn loser) = "" then parenize left_tn
     else "(" ^ (drop left_tn loser) ^ " " ^ (drop right_tn loser) ^ ")";;
