type ae =
  | CONST of int
  | VAR of string
  | POWER of string * int
  | TIMES of ae list
  | SUM of ae list

exception Error of string

let rec diff (alg_expr:ae) (respect_to:string): ae =
  match alg_expr with
  | CONST c -> CONST 0
  | VAR var -> if var = respect_to then CONST 1 else CONST 0
  | POWER (var, order) -> if var = respect_to then 
                            TIMES ((CONST order)::(POWER (var, order - 1))::[])
                          else CONST 0
  | TIMES [] -> raise (Error "Empty TIMES...")
  | TIMES (var::[]) -> (diff var respect_to)
  | TIMES (var::remains) -> 
     let left_diff = TIMES ((diff var respect_to)::remains) in
     let right_diff = TIMES (var::(diff (TIMES remains) respect_to)::[]) in
     SUM (left_diff::right_diff::[])
  | SUM [] -> raise (Error "Empty SUM...")
  | SUM (term::[]) ->  (diff term respect_to)
  | SUM (term::remains) -> SUM ((diff term respect_to)::(diff (SUM remains) respect_to)::[])
