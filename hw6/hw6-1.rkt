#lang racket

;;; If these statements are omitted, your submission will be graded 0.
(provide memo-ways)

(define (insert db n m val)
  (if (assoc (list n m) db)
      db
      (append db (list (list (list n m) val)))))

(define (memoize f)
  (let ((db '()))
    (lambda (n m)
      (let ((select (assoc (list n m) db)))
        (if select
            (cadr select)
            (let ((result (f n m)))
              (set! db (insert db n m result))
              result))))))


(define memo-ways ; memo-ways: int * int -> int
  
  (memoize (lambda (n m)
             (cond ((= n 0) 1)
                   ((= m 0) 1)
                   (else (+ (memo-ways (- n 1) m)
                            (memo-ways n (- m 1))))))))
