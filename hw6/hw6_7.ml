exception TODO

type crazy2 =
  | NIL
  | ZERO of crazy2
  | ONE of crazy2
  | MONE of crazy2

let rec crazy2add (left_cr: crazy2) (right_cr: crazy2): crazy2 =
  match (left_cr, right_cr) with
  | (NIL, _) -> right_cr
  | (_, NIL) -> left_cr
  | (ZERO left_crazy, ZERO right_crazy) -> ZERO (crazy2add left_crazy right_crazy)
  | (ZERO left_crazy, ONE right_crazy) -> ONE (crazy2add left_crazy right_crazy)
  | (ZERO left_crazy, MONE right_crazy) -> MONE (crazy2add left_crazy right_crazy)

  | (ONE left_crazy, ZERO right_crazy) -> ONE (crazy2add left_crazy right_crazy)
  | (ONE left_crazy, ONE right_crazy) -> ZERO (crazy2add (ONE NIL) (crazy2add left_crazy right_crazy))
  | (ONE left_crazy, MONE right_crazy) -> ZERO (crazy2add left_crazy right_crazy)

  | (MONE left_crazy, ZERO right_crazy) -> MONE (crazy2add left_crazy right_crazy)
  | (MONE left_crazy, MONE right_crazy) -> ZERO (crazy2add (MONE NIL) (crazy2add left_crazy right_crazy))
  | (MONE left_crazy, ONE right_crazy) -> ZERO (crazy2add left_crazy right_crazy)
