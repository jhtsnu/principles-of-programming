type formula = 
  | TRUE
  | FALSE
  | NOT of formula
  | ANDALSO of formula * formula
  | ORELSE of formula * formula
  | IMPLY of formula * formula
  | LESS of expr * expr
and expr =
  | NUM of int
  | PLUS of expr * expr
  | MINUS of expr * expr

let rec eval (fmla: formula): bool =

  let rec calc(e: expr): int = 
    match e with
    | NUM num -> num
    | PLUS (left_expr, right_expr) -> (calc left_expr) + (calc right_expr)
    | MINUS (left_expr, right_expr) -> (calc left_expr) - (calc right_expr) 
  in 

  match fmla with
  | TRUE -> true
  | FALSE -> false
  | NOT f -> not (eval f)
  | ANDALSO (left_f, right_f) -> (eval left_f) && (eval right_f)
  | ORELSE (left_f, right_f) -> (eval left_f) || (eval right_f)
  | IMPLY (left_f, right_f) -> not (eval left_f) || (eval right_f)
  | LESS (left_expr, right_expr) -> (calc left_expr) < (calc right_expr);;
