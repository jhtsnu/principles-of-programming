module type SMATCH = sig
  type c =
    | Zero
    | One
    | Two
    | Mult of c * c
    | Sum of c * c
    | Opt of c (* c? *)
    | Star of c (* c* *)

  val smatch: string -> c -> bool
end

module Smatch : SMATCH = struct
  exception ETODO

  type c =
    | Zero
    | One
    | Two
    | Mult of c * c
    | Sum of c * c
    | Opt of c (* c? *)
    | Star of c (* c* *)

  let rec smatch: string -> c -> bool =
    fun str code ->
    match code with 
    | Zero -> str = "0"
    | One -> str = "1"
    | Two -> str = "2"
    | Mult (a_code, b_code) ->
       let str_len = (String.length str) in
       let rec loop(n:int): bool = 
         if n <= str_len then
           let prefix = (String.sub str 0 n) in
           let postfix = (String.sub str n (str_len - n)) in
           if ((smatch prefix a_code) && (smatch postfix b_code)) then
             true
           else loop(n+1)
         else false
       in
       loop(0)
    | Sum (a_code, b_code) -> (smatch str a_code) || (smatch str b_code)
    | Opt (a_code) -> str = "" || (smatch str a_code)
    | Star (a_code) -> 
       let rec loop(n:int) (sub_str:string): bool = 
         if n <= (String.length sub_str) then
           let prefix = (String.sub sub_str 0 n) in
           let postfix = (String.sub sub_str n ((String.length sub_str) - n)) in
           if (smatch prefix a_code) then
             (postfix = "") || (loop 1 postfix)
           else loop (n+1) sub_str
         else false
       in
       (str = "") || (loop 1 str)
end
