module type SKI = sig
    type liquid =
      | S
      | K
      | I
      | V of string (* varible *)
      | M of liquid * liquid (* mix of two liquids *)
    val react: liquid -> liquid
    val pprint: liquid -> string
  end

module SkiLiquid : SKI = struct
  type liquid =
    | S
    | K
    | I
    | V of string (* varible *)
    | M of liquid * liquid (* mix of two liquids *)

  let rec react: liquid -> liquid =
    let reactElse (liq:liquid): liquid =
      match liq with
      | M (left_liq, right_liq) -> 
         let reacted = M (react left_liq, react right_liq) in
         if reacted = liq then
           reacted
         else react reacted
      | _ -> liq
    in
    fun liq ->
    match liq with
    | S -> S
    | K -> K
    | I -> I
    | V var -> V var
    | M (left_liq, right_liq)->
       (match (left_liq, right_liq) with
        | (I, _) -> react right_liq (* I reacts *)
        | (M (lleft_liq, lright_liq), _) ->
           (match (lleft_liq, lright_liq) with
            | (K, _) -> react lright_liq  (* K reacts *)
            | (M (llleft_liq, llright_liq), _) ->
               (match (llleft_liq, llright_liq) with
                | (S, _) -> react (M (M (llright_liq, right_liq), (* S reacts *)
                                      M (lright_liq, right_liq)))
                | (_, _) -> reactElse liq
               )
            | (_, _) -> reactElse liq)
        | (_, _) -> reactElse liq)

  let rec pprint: liquid -> string =
    fun l ->
    match l with
    | S -> "S"
    | K -> "K"
    | I -> "I"
    | V var -> var
    | M (left_liq, right_liq) -> "(" ^ (pprint left_liq) ^ " " ^ (pprint right_liq) ^ ")"

end
