(* TA's comment:
 * 1. Implement step_tm as well as run_tm.
 *
 * 2. Functions print_tape and print_tm has different type from the direction.
 * Follow the types in this skeleton code.
 *)

module type TM = sig
    type symbol = string
    type move = Right | Left | Stay
    type todo = Erase | Write of symbol
    type state = string
    type rule = state * symbol * todo * move * state
    type ruletable = rule list
    type tape
    type tm
    (* tape part *)
    val init_tape: symbol list -> tape
    val read_tape: tape -> symbol
    val write_tape: tape -> symbol -> tape
    val move_tape_left: tape -> tape
    val move_tape_right: tape -> tape
    val print_tape: tape -> int -> string (* instead of tape -> unit *)
    (* rule table part *)
    val match_rule: state -> symbol -> ruletable -> (todo * move * state) option
    (* instead of state -> symbol -> ruletable -> todo * move * state *)
    (* main *)
    val make_tm: symbol list -> state -> ruletable -> tm
    (* instead of symbol list -> state list -> state -> ruletable -> tm *)
    val step_tm: tm -> tm option (* You should implement this. *)
    val run_tm: tm -> tm
    val print_tm: tm -> int -> string (* instead of tm -> int -> unit *)
  end

module TuringMachine : TM = struct
  exception RUN_TM_FAILED
  type symbol = string
  type move = Right | Left | Stay
  type todo = Erase | Write of symbol
  type state = string
  type rule = state * symbol * todo * move * state
  type ruletable = rule list

  type tape = Tape of int * symbol list
  type tm = Tm of tape * state * ruletable

  let rec make_list (s: symbol) (n: int): symbol list =
    if n = 0 then
      []
    else
      [s] @ (make_list s (n - 1))

  let rec take (symbols:symbol list) (n: int): symbol list =
    if n = 0 then [] else
      match symbols with
      | head::remains -> head:: take remains (n - 1)
      | [] -> []
  let rec drop (symbols:symbol list) (n: int): symbol list =
    if n = 0 then symbols else
      match symbols with
      | head::remains -> drop remains (n - 1)
      | [] -> []


  (* tape part *)
  let init_tape: symbol list -> tape =
    fun symbols ->
    match symbols with
    | [] -> Tape (0, ["-"])
    | _ -> Tape (0, symbols)

  let read_tape: tape -> symbol =
    fun tape ->
    match tape with
    | Tape (head_ref, symbols) when head_ref >= 0
                                    && head_ref < List.length symbols ->
       List.nth symbols head_ref
    | Tape (head_ref, symbols) -> "-"

  let write_tape: tape -> symbol -> tape =
    fun tape s ->
    match tape with
    | Tape (head_ref, symbols) when head_ref < 0 ->
       Tape (0, [s] @ make_list "-" (-head_ref - 1) @ symbols)
    | Tape (head_ref, symbols) when head_ref < List.length symbols ->
       Tape (head_ref, (take symbols head_ref) @ [s] @ (drop symbols (head_ref + 1)))
    | Tape (head_ref, symbols) ->
       Tape (head_ref, symbols @ make_list  "-" (head_ref - List.length symbols) @ [s])

  let move_tape_left: tape -> tape =
    fun tape ->
    match tape with
    | Tape (head_ref, symbols) -> Tape (head_ref + 1, symbols)

  let move_tape_right: tape -> tape =
    fun tape ->
    match tape with
    | Tape (head_ref, symbols) -> Tape (head_ref - 1, symbols)

  let print_tape: tape -> int -> string = (* instead of tape -> unit *)
    fun tape size ->
    let total_length = size * 2 + 1 in
    let bound(n: int): int = max (n) 0 in
    let rec lst2str(str_lst: string list): string =
      match str_lst with
      | [] -> ""
      | tail::[] -> tail
      | head::remains -> head ^ "." ^ (lst2str remains)
    in
    match tape with
    | Tape (head_ref, symbols) ->
       let prior_blank_length = bound (size - head_ref) in
       let post_blank_length = bound (size - ((List.length symbols) - head_ref) + 1) in
       lst2str ((make_list "-" prior_blank_length)
                @ (take (drop symbols (bound (head_ref - size))) (total_length - post_blank_length - prior_blank_length))
                @ (make_list "-" post_blank_length))

  (* rule table part *)
  let match_rule: state -> symbol -> ruletable -> (todo * move * state) option =
    fun st sym rules ->
    match (List.find (fun rule ->
                      match rule with
                      | (curr_st, curr_sym, _, _, _) ->
                         curr_st = st && curr_sym = sym)
                     rules) with
    | (_, _, td, mv, next_st) ->
       Some (td, mv, next_st)

  let make_tm: symbol list -> state -> ruletable -> tm =
    fun symbols initial_state rules ->
    Tm ((init_tape symbols), initial_state, rules)

  let step_tm: tm -> tm option =
    fun tm ->
    match tm with
    | Tm (tp, st, rules) ->
       try
         let rule = match_rule st (read_tape tp) rules in
         let todo_tp(tp: tape) (td:todo): tape =
           (match td with
            | Erase -> write_tape tp "-"
            | Write (sym) -> write_tape tp sym)
         in
         let move_tp(tp: tape) (mv:move): tape =
           (match mv with
            | Right -> move_tape_left tp
            | Left ->  move_tape_right tp
            | Stay -> tp)
         in
         (match rule with
          | Some (td, mv, next_st) ->
             let next_tape = (move_tp (todo_tp tp td) mv) in
             Some (Tm (next_tape, next_st, rules))
          | None -> None)
       with Not_found -> None

  let print_tm: tm -> int -> string = (* instead of tm -> int -> unit *)
    fun tm size ->
    match tm with
    | Tm (tp, _, _) ->
       print_tape tp size

  let rec run_tm: tm -> tm =
    fun tm ->
    let stepped_tm = step_tm tm in
    match stepped_tm with
    | Some stm -> 
       if stm = tm then tm
       else run_tm stm
    | None -> tm

end
