#lang racket

; Provide for other module.
; If you comment it, your self-grader will not work well.
(provide helloWorld)

; Define the helloWorld function.
(define (helloWorld)
  "Hello, World!\n")

; Uncomment the following line and see what happens.
; Comment it before submission.
; (printf (helloWorld))
