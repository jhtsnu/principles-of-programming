module type Queue =
  sig
    type element
    type queue
    exception EMPTY_Q
    val emptyq: queue
    val enq: queue * element -> queue
    val deq: queue -> element * queue
  end

module StringSetQ : (Queue with type element = string) = struct
  type element = string
  type queue = (element list * element list)
  exception EMPTY_Q
  let emptyq = ([], [])

  let enq ((q:queue), (el:element)) =
    match q with
    | (leftQ, rightQ) ->
       if (List.mem el leftQ) || (List.mem el rightQ) then
         (leftQ, rightQ)
       else
         ((el::leftQ), rightQ)

  let deq (q:queue) =
    match q with
    | (leftQ, []) ->
       (match (List.rev leftQ) with
        | [] -> raise EMPTY_Q
        | head_leftQ::[] -> (head_leftQ, emptyq)
        | head_leftQ::remains_leftQ -> (head_leftQ, ([], remains_leftQ)))
    | (leftQ, head_rightQ::remains_rightQ) -> (head_rightQ, (leftQ, remains_rightQ))

end

module StringSetQQ : (Queue with type element = StringSetQ.queue) = struct
  type element = StringSetQ.queue
  type queue = (element list * element list)
  exception EMPTY_Q
  let emptyq = ([], [])


  let rec queue2list (q:StringSetQ.queue) : string list =
    try let (e,r) = StringSetQ.deq q in
        e::(queue2list r)
    with StringSetQ.EMPTY_Q -> []

  let enq ((q:queue), (el:element)) =
    let isQsame(another_el:element): bool = 
      (queue2list another_el) = (queue2list el)
    in
    match q with
    | (leftQ, rightQ) ->
       if (List.exists isQsame leftQ) || (List.exists isQsame rightQ) then
         (leftQ, rightQ)
       else
         ((el::leftQ), rightQ)

  let deq (q:queue) =
    match q with
    | (leftQ, []) ->
       (match (List.rev leftQ) with
        | [] -> raise EMPTY_Q
        | head_leftQ::[] -> (head_leftQ, emptyq)
        | head_leftQ::remains_leftQ -> (head_leftQ, ([], remains_leftQ)))
    | (leftQ, head_rightQ::remains_rightQ) -> (head_rightQ, (leftQ, remains_rightQ))

end
