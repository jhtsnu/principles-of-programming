module type Queue = sig
    type element
    type queue
    exception EMPTY_Q
    val emptyq: queue
    val enq: queue * element -> queue
    val deq: queue -> element * queue
  end

module type ArgTy = sig
    type t
    val is_eq : t -> t -> bool
  end

module QueueMake (Arg: ArgTy) : (Queue with type element = Arg.t) = struct
  type element = Arg.t
  type queue = (element list * element list)
  exception EMPTY_Q
  let emptyq = ([], [])
  let enq ((q:queue), (el:element)) =
    let isQsame(another_el:element): bool = 
      Arg.is_eq another_el el
    in
    match q with
    | (leftQ, rightQ) ->
       if (List.exists isQsame leftQ) || (List.exists isQsame rightQ) then
         (leftQ, rightQ)
       else
         ((el::leftQ), rightQ)

  let deq (q:queue) =
    match q with
    | (leftQ, []) ->
       (match (List.rev leftQ) with
        | [] -> raise EMPTY_Q
        | head_leftQ::[] -> (head_leftQ, emptyq)
        | head_leftQ::remains_leftQ -> (head_leftQ, ([], remains_leftQ)))
    | (leftQ, head_rightQ::remains_rightQ) -> (head_rightQ, (leftQ, remains_rightQ))
end
