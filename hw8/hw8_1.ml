module type Queue = sig
    type element
    type queue
    exception EMPTY_Q
    val emptyq: queue
    val enq: queue * element -> queue
    val deq: queue -> element * queue
  end

module StringQ: (Queue with type element = string) = struct
  type element = string
  type queue = (element list * element list)
  exception EMPTY_Q
  let emptyq = ([], [])

  let enq ((q:queue), (el:element)) =
    match q with
    | (leftQ, rightQ) -> ((el::leftQ), rightQ)

  let deq (q:queue) =
    match q with
    | (leftQ, []) ->
       (match (List.rev leftQ) with
        | [] -> raise EMPTY_Q
        | head_leftQ::[] -> (head_leftQ, emptyq)
        | head_leftQ::remains_leftQ -> (head_leftQ, ([], remains_leftQ)))
    | (leftQ, head_rightQ::remains_rightQ) -> (head_rightQ, (leftQ, remains_rightQ))
end

module StringQQ : (Queue with type element = StringQ.queue) = struct
  type element = StringQ.queue
  type queue = (element list * element list)
  exception EMPTY_Q
  let emptyq = ([], [])

  let enq ((q:queue), (el:element)) =
    match q with
    | (leftQ, rightQ) -> ((el::leftQ), rightQ)

  let deq (q:queue) =
    match q with
    | (leftQ, []) ->
       (match (List.rev leftQ) with
        | [] -> raise EMPTY_Q
        | head_leftQ::[] -> (head_leftQ, emptyq)
        | head_leftQ::remains_leftQ -> (head_leftQ, ([], remains_leftQ)))
    | (leftQ, head_rightQ::remains_rightQ) -> (head_rightQ, (leftQ, remains_rightQ))
end
