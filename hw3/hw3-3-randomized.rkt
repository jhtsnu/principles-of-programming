#lang racket

(require "hw3-3-library.rkt")

(provide mazeGen)

(define (mazeGen n m)

  ;; ArrowCode
  (define S 0)
  (define N 1)
  (define SE 2)
  (define SW 3)
  (define NE 4)
  (define NW 5)
  (define All-ArrowCode '(0 1 2 3 4 5))

  ;; Direction
  (define (get-direction arrow-code)
    (cond [(= arrow-code S)  open-s]
          [(= arrow-code N)  open-n]
          [(= arrow-code SE) open-se]
          [(= arrow-code SW) open-sw]
          [(= arrow-code NE) open-ne]
          [(= arrow-code NW) open-nw]))

  ;; Position
  (define Position cons)
  (define x-pos car)
  (define y-pos cdr)
  (define (add-pos pos1 pos2)
    (Position (+ (x-pos pos1) (x-pos pos2))
              (+ (y-pos pos1) (y-pos pos2))))

  (define (get-delta direction)
    (cond [(eq? direction S)  (Position  1  0)]
          [(eq? direction N)  (Position -1  0)]
          [(eq? direction SE) (Position  1  1)]
          [(eq? direction SW) (Position  1 -1)]
          [(eq? direction NE) (Position -1  1)]
          [(eq? direction NW) (Position -1 -1)]))

  (define (move pos direction)
    (add-pos pos (get-delta direction))

  ;; Rooms
  (define empty-set '()) 
  (define add-element cons) 
  (define (is-member? room roomset) 
    (cond ((null? roomset) #f) 
          ((equal? room (car roomset)) #t) 
          (else (is-member? room (cdr roomset))))) 
  (define same-room? equal?) 
  (define union-rooms append)
  
  (define (is-valid-pos? pos)
    (let ((x (x-pos pos))
          (y (y-pos pos)))
      (and (<= 0 x) (< x n) (<= 0 y) (< y m))))

  (let ((maze (init-maze n m)))
    ;; pos x room set x room set x maze
    (letrec 
        ((randomized-prim-algorithm
          (lambda (visited-rooms
                   willvisit-rooms
                   maze)
            (let* (;;
                   (current-pos (car visited-rooms))
                   ;; 
                   (directions-to-move
                    (filter-map
                     (lambda (arrow-code)
                       (let ((direction (get-direction arrow-code))
                             (next-step (move current-pos direction)))
                         (and (is-valid-pos? next-step)
                              (not (is-member? next-step visited-rooms))
                              direction)))
                     All-ArrowCode))
                   ;;
                   (discovred-rooms
                    (map (lambda (direction)
                           (move current-pos direction))
                         directions-to-move))
                   ;;
                   (willvisit-rooms (union-rooms
                                     discovred-rooms
                                     willvisit-rooms)))
              (foldl
               (lambda (visiting-room result)
                 (randomized-prim-algorithm
                  (add-element visiting-room visited-rooms)
                  willvisit-rooms
                  ((open-direction visiting-room) maze)))
               maze
               willvisit-rooms)))))
      (randomized-prim-algorithm 
       (add-element (Position 0 0) empty-set)
       empty-set
       maze))))


(mazeGen 3 3)
